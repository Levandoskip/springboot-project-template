package com.example.service.hello;

import com.example.service.hello.api.World;
import com.example.service.hello.entity.WorldEntity;
import com.example.service.hello.entity.WorldRepository;
import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

@RestController
@RequestMapping("/hello")
public class HelloController {

  private WorldRepository worldRepostiory;

  @Autowired
  public HelloController(WorldRepository worldRepo) {
    this.worldRepostiory = worldRepo;
  }

  @RequestMapping(path="/world", method= RequestMethod.POST)
  public ResponseEntity<World> createWorld(@RequestBody World world) {
    try {
      WorldEntity entity = WorldMapper.fromApi(world);

      Long id = worldRepostiory.save(entity).getId();
      URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(id).toUri();

      return ResponseEntity.created(uri).body(null);
    } catch (Exception e) {
      return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
    }
  }

  @RequestMapping(path="/world", method= RequestMethod.GET)
  public ResponseEntity<List<World>> getAllWorlds() {
    try {

      List<World> worlds = StreamSupport.stream(worldRepostiory.findAll().spliterator(), false)
          .map( WorldMapper::fromEntity ).collect(Collectors.toList());

      return ResponseEntity.ok(worlds);
    } catch (Exception e) {
      return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
    }
  }

  @RequestMapping(path="/world/{id}", method= RequestMethod.PUT)
  public ResponseEntity<World> updateWorld(@PathVariable Long id, @RequestBody World updateWorld) {
    try {
      WorldEntity entity = worldRepostiory.findOne(id);
      entity.updateFrom(updateWorld);

      worldRepostiory.save(entity);

      return ResponseEntity.status(HttpStatus.ACCEPTED).body(null);
    } catch (Exception e) {
      return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
    }
  }


  @RequestMapping(path="/world/{id}", method= RequestMethod.GET)
  public ResponseEntity<World> getWorld(@PathVariable Long id) {
    try {
        return ResponseEntity.ok(WorldMapper.fromEntity(worldRepostiory.findOne(id)));
    } catch (Exception e) {
      return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
    }
  }

  @RequestMapping(path="/world/{id}", method= RequestMethod.DELETE)
  public ResponseEntity<World> deleteWorld(@PathVariable Long id) {
    try {
      worldRepostiory.delete(id);
      return ResponseEntity.accepted().body(null);
    } catch (Exception e) {
      return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
    }
  }


}
