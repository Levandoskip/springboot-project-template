package com.example.service.hello.entity;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;

/**
 * A Repostiory provides a simple abstraction for
 */
public interface WorldRepository extends CrudRepository<WorldEntity, Long> {
}
