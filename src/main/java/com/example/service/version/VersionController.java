package com.example.service.version;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Respond with a version number for the running service
 */
@RestController
public class VersionController {
  @RequestMapping("/version")
  public ResponseEntity<String> version() {
    return ResponseEntity.ok(getClass().getPackage().getImplementationVersion());
  }
}
